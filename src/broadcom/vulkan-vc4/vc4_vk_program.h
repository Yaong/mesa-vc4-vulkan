/*
 * Copyright (C) 2019-2020 Yaong <yaongtime@gmail.com>
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef VC4_VK_PROGRAM_H
#define VC4_VK_PROGRAM_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "pipe/p_state.h"
#include "vc4_vk_cl.h"

#ifdef USE_VC4_SIMULATOR
#define using_vc4_simulator true
#else
#define using_vc4_simulator false
#endif

#define VC4_DIRTY_BLEND         (1 <<  0)
#define VC4_DIRTY_RASTERIZER    (1 <<  1)
#define VC4_DIRTY_ZSA           (1 <<  2)
#define VC4_DIRTY_FRAGTEX       (1 <<  3)
#define VC4_DIRTY_VERTTEX       (1 <<  4)

#define VC4_DIRTY_BLEND_COLOR   (1 <<  7)
#define VC4_DIRTY_STENCIL_REF   (1 <<  8)
#define VC4_DIRTY_SAMPLE_MASK   (1 <<  9)
#define VC4_DIRTY_FRAMEBUFFER   (1 << 10)
#define VC4_DIRTY_STIPPLE       (1 << 11)
#define VC4_DIRTY_VIEWPORT      (1 << 12)
#define VC4_DIRTY_CONSTBUF      (1 << 13)
#define VC4_DIRTY_VTXSTATE      (1 << 14)
#define VC4_DIRTY_VTXBUF        (1 << 15)

#define VC4_DIRTY_SCISSOR       (1 << 17)
#define VC4_DIRTY_FLAT_SHADE_FLAGS (1 << 18)
#define VC4_DIRTY_PRIM_MODE     (1 << 19)
#define VC4_DIRTY_CLIP          (1 << 20)
#define VC4_DIRTY_UNCOMPILED_VS (1 << 21)
#define VC4_DIRTY_UNCOMPILED_FS (1 << 22)
#define VC4_DIRTY_COMPILED_CS   (1 << 23)
#define VC4_DIRTY_COMPILED_VS   (1 << 24)
#define VC4_DIRTY_COMPILED_FS   (1 << 25)
#define VC4_DIRTY_FS_INPUTS     (1 << 26)
#define VC4_DIRTY_UBO_1_SIZE    (1 << 27)

#define VC4_QIR_UBO_BUF_INDEX_OFFSET_BITS (24)
#define VC4_QIR_UBO_BUF_OFFSET_BITS       ((1 << VC4_QIR_UBO_BUF_INDEX_OFFSET_BITS) - 1)

struct vc4_sampler_view {
        struct pipe_sampler_view base;
        uint32_t texture_p0;
        uint32_t texture_p1;
        bool force_first_level;
        /**
         * Resource containing the actual texture that will be sampled.
         *
         * We may need to rebase the .base.texture resource to work around the
         * lack of GL_TEXTURE_BASE_LEVEL, or to upload the texture as tiled.
         */
        // struct pipe_resource *texture;
        struct vc4_image *image;
};

struct vc4_sampler_state {
        struct pipe_sampler_state base;
        uint32_t texture_p1;
};

struct vc4_texture_stateobj {
        struct vc4_sampler_view *textures[PIPE_MAX_SAMPLERS];
        unsigned num_textures;
        struct vc4_sampler_state *samplers[PIPE_MAX_SAMPLERS];
        unsigned num_samplers;
};

struct vc4_shader_uniform_info {
        enum quniform_contents *contents;
        uint32_t *data;
        uint32_t count;
        uint32_t num_texture_samples;
};

struct vc4_uncompiled_shader {
        /** A name for this program, so you can track it in shader-db output. */
        uint32_t program_id;
        /** How many variants of this program were compiled, for shader-db. */
        uint32_t compiled_variant_count;
        struct pipe_shader_state base;
};

struct vc4_rasterizer_state {
        struct pipe_rasterizer_state base;

        /* VC4_CONFIGURATION_BITS */
        uint8_t config_bits[V3D21_CONFIGURATION_BITS_length];

        struct PACKED {
                uint8_t depth_offset[V3D21_DEPTH_OFFSET_length];
                uint8_t point_size[V3D21_POINT_SIZE_length];
                uint8_t line_width[V3D21_LINE_WIDTH_length];
        } packed;

        /** Raster order flags to be passed in struct drm_vc4_submit_cl.flags. */
        uint32_t tile_raster_order_flags;
};

struct vc4_depth_stencil_alpha_state {
        struct pipe_depth_stencil_alpha_state base;

        /* VC4_CONFIGURATION_BITS */
        uint8_t config_bits[V3D21_CONFIGURATION_BITS_length];

        /** Uniforms for stencil state.
         *
         * Index 0 is either the front config, or the front-and-back config.
         * Index 1 is the back config if doing separate back stencil.
         * Index 2 is the writemask config if it's not a common mask value.
         */
        uint32_t stencil_uniforms[3];
};

struct vc4_fs_inputs {
        /**
         * Array of the meanings of the VPM inputs this shader needs.
         *
         * It doesn't include those that aren't part of the VPM, like
         * point/line coordinates.
         */
        struct vc4_varying_slot *input_slots;
        uint32_t num_inputs;
};

struct vc4_compiled_shader {
        uint64_t program_id;
        struct vc4_bo *bo;

        struct vc4_shader_uniform_info uniforms;

        /**
         * VC4_DIRTY_* flags that, when set in vc4->dirty, mean that the
         * uniforms have to be rewritten (and therefore the shader state
         * reemitted).
         */
        uint32_t uniform_dirty_bits;

        /** bitmask of which inputs are color inputs, for flat shade handling. */
        uint32_t color_inputs;

        bool disable_early_z;

        /* Set if the compile failed, likely due to register allocation
         * failure.  In this case, we have no shader to run and should not try
         * to do any draws.
         */
        bool failed;

        bool fs_threaded;

        uint8_t num_inputs;

        /* Byte offsets for the start of the vertex attributes 0-7, and the
         * total size as "attribute" 8.
         */
        uint8_t vattr_offsets[9];
        uint8_t vattrs_live;

        const struct vc4_fs_inputs *fs_inputs;
};
struct vc4_pipe_constant_buffer
{
   unsigned buffer_offset; /**< offset to start of data in buffer, in bytes */
   unsigned buffer_size;   /**< how much data can be read in shader */
   const void *user_buffer;  /**< pointer to a user buffer if buffer == NULL */
};

struct vc4_constbuf_stateobj {
        struct vc4_pipe_constant_buffer cb[PIPE_MAX_CONSTANT_BUFFERS];
        uint32_t enabled_mask;
        uint32_t dirty_mask;
};

struct vc4_program_stateobj {
        struct vc4_uncompiled_shader bind_vs, bind_fs;
        struct vc4_compiled_shader *cs, *vs, *fs;
};

struct vc4_vertex_stateobj {
        struct pipe_vertex_element pipe[PIPE_MAX_ATTRIBS];
        unsigned num_elements;
};

struct vc4_framebuffer_info {
        enum pipe_format format : 16;
};

struct vc4_framebuffer_state {
        uint32_t nr_cbufs;

        struct vc4_framebuffer_info cbufs[PIPE_MAX_COLOR_BUFS];
        struct vc4_framebuffer_info zsbuf; /**< Z/stencil buffer */
};

struct vc4_vertex_buffer {
        uint32_t stride;        /**< stride to same attrib in next vertex, in bytes */
        unsigned buffer_offset; /**< offset to start of data in buffer, in bytes */
        struct vc4_buffer *buffer;
};

struct vc4_vertexbuf_stateobj {
        struct vc4_vertex_buffer vb[PIPE_MAX_ATTRIBS];
        unsigned count;
        uint32_t enabled_mask;
        uint32_t dirty_mask;
};

struct vc4_context {
    struct vc4_device *device;

    struct vc4_program_stateobj prog;

    struct vc4_vertex_stateobj *vtx;
    struct vc4_vertexbuf_stateobj *vertexbuf;

    struct vc4_buffer *index_shadow;

    struct vc4_vk_job *job;

    /* Current pipeline state objects */
    struct pipe_scissor_state scissor;
    struct pipe_blend_state *blend;
    struct vc4_rasterizer_state *rasterizer;

    struct vc4_depth_stencil_alpha_state *zsa;
    struct pipe_stencil_ref stencil_ref;

    struct vc4_texture_stateobj verttex, fragtex;

    struct vc4_framebuffer_state framebuffer;
    struct pipe_viewport_state viewport;
//     struct vc4_constbuf_stateobj constbuf[PIPE_SHADER_TYPES];
    struct vc4_constbuf_stateobj constbuf;

    struct hash_table *fs_cache, *vs_cache;
    struct set *fs_inputs_set;
    uint32_t next_uncompiled_program_id;
    uint64_t next_compiled_program_id;

    struct ra_regs *regs;
    unsigned int reg_class_any[2];
    unsigned int reg_class_a_or_b[2];
    unsigned int reg_class_a_or_b_or_acc[2];
    unsigned int reg_class_r0_r3;
    unsigned int reg_class_r4_or_a[2];
    unsigned int reg_class_a[2];

//     struct pipe_clip_state clip;

    /** Maximum index buffer valid for the current shader_rec. */
    uint32_t max_index;
    /** Last index bias baked into the current shader_rec. */
    uint32_t last_index_bias;

    unsigned sample_mask;

    /** bitfield of VC4_DIRTY_* */
    uint32_t dirty;
};

static inline struct vc4_sampler_view *
vc4_sampler_view(struct pipe_sampler_view *psview)
{
        return (struct vc4_sampler_view *)psview;
}

void vc4_set_shader_uniform_dirty_flags(struct vc4_compiled_shader *shader);
struct qpu_reg *vc4_register_allocate(struct vc4_context *vc4, struct vc4_compile *c);
void vc4_generate_code(struct vc4_context *vc4, struct vc4_compile *c);
bool vc4_update_compiled_shaders(struct vc4_context *vc4, uint8_t prim_mode);
void vc4_program_init(struct vc4_context *vc4);
void vc4_program_fini(struct vc4_context *vc4);
void vc4_emit_state(struct vc4_context *vc4);
void vc4_write_uniforms(struct vc4_context *vc4, struct vc4_compiled_shader *shader,
                   struct vc4_constbuf_stateobj *cb,
                   struct vc4_texture_stateobj *texstate);

#endif