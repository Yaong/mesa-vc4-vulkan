/*
 * Copyright © 2016 Red Hat
 * based on intel anv code:
 * Copyright © 2015 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "vc4_private.h"

#include "vk_util.h"
#include "wsi_common.h"
#include "drm-uapi/drm_fourcc.h"

#include "wsi_common.h"

static VKAPI_PTR PFN_vkVoidFunction
vc4_wsi_proc_addr(VkPhysicalDevice physicalDevice, const char *pName)
{
   return vc4_lookup_entrypoint_unchecked(pName);
}

VkResult
vc4_wsi_init(struct vc4_physical_device *physical_device)
{
   VkResult result;

   result = wsi_device_init(&physical_device->wsi_device,
                            vc4_physical_device_to_handle(physical_device),
                            vc4_wsi_proc_addr,
                            &physical_device->instance->alloc,
                            physical_device->master_fd, NULL, false);
   if (result != VK_SUCCESS)
      return result;

   physical_device->wsi_device.supports_modifiers = true;

   return VK_SUCCESS;
}

void
vc4_wsi_finish(struct vc4_physical_device *physical_device)
{
   wsi_device_finish(&physical_device->wsi_device,
                     &physical_device->instance->alloc);
}

void
vc4_DestroySurfaceKHR(VkInstance _instance,
                     VkSurfaceKHR _surface,
                     const VkAllocationCallbacks *pAllocator)
{
   VC4_FROM_HANDLE(vc4_instance, instance, _instance);
   ICD_FROM_HANDLE(VkIcdSurfaceBase, surface, _surface);

   vk_free2(&instance->alloc, pAllocator, surface);
}

VkResult
vc4_GetPhysicalDeviceSurfaceSupportKHR(VkPhysicalDevice physicalDevice,
                                      uint32_t queueFamilyIndex,
                                      VkSurfaceKHR surface,
                                      VkBool32 *pSupported)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_surface_support(
      &device->wsi_device, queueFamilyIndex, surface, pSupported);
}

VkResult
vc4_GetPhysicalDeviceSurfaceCapabilitiesKHR(
   VkPhysicalDevice physicalDevice,
   VkSurfaceKHR surface,
   VkSurfaceCapabilitiesKHR *pSurfaceCapabilities)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_surface_capabilities(&device->wsi_device, surface,
                                              pSurfaceCapabilities);
}

VkResult
vc4_GetPhysicalDeviceSurfaceCapabilities2KHR(
   VkPhysicalDevice physicalDevice,
   const VkPhysicalDeviceSurfaceInfo2KHR *pSurfaceInfo,
   VkSurfaceCapabilities2KHR *pSurfaceCapabilities)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_surface_capabilities2(
      &device->wsi_device, pSurfaceInfo, pSurfaceCapabilities);
}

VkResult
vc4_GetPhysicalDeviceSurfaceCapabilities2EXT(
   VkPhysicalDevice physicalDevice,
   VkSurfaceKHR surface,
   VkSurfaceCapabilities2EXT *pSurfaceCapabilities)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_surface_capabilities2ext(
      &device->wsi_device, surface, pSurfaceCapabilities);
}

VkResult
vc4_GetPhysicalDeviceSurfaceFormatsKHR(VkPhysicalDevice physicalDevice,
                                      VkSurfaceKHR surface,
                                      uint32_t *pSurfaceFormatCount,
                                      VkSurfaceFormatKHR *pSurfaceFormats)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_surface_formats(
      &device->wsi_device, surface, pSurfaceFormatCount, pSurfaceFormats);
}

VkResult
vc4_GetPhysicalDeviceSurfaceFormats2KHR(
   VkPhysicalDevice physicalDevice,
   const VkPhysicalDeviceSurfaceInfo2KHR *pSurfaceInfo,
   uint32_t *pSurfaceFormatCount,
   VkSurfaceFormat2KHR *pSurfaceFormats)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_surface_formats2(&device->wsi_device, pSurfaceInfo,
                                          pSurfaceFormatCount,
                                          pSurfaceFormats);
}

VkResult
vc4_GetPhysicalDeviceSurfacePresentModesKHR(VkPhysicalDevice physicalDevice,
                                           VkSurfaceKHR surface,
                                           uint32_t *pPresentModeCount,
                                           VkPresentModeKHR *pPresentModes)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_surface_present_modes(
      &device->wsi_device, surface, pPresentModeCount, pPresentModes);
}

VkResult
vc4_CreateSwapchainKHR(VkDevice _device,
                      const VkSwapchainCreateInfoKHR *pCreateInfo,
                      const VkAllocationCallbacks *pAllocator,
                      VkSwapchainKHR *pSwapchain)
{
   VC4_FROM_HANDLE(vc4_device, device, _device);
   const VkAllocationCallbacks *alloc;
   if (pAllocator)
      alloc = pAllocator;
   else
      alloc = &device->vk.alloc;

   return wsi_common_create_swapchain(&device->physical_device->wsi_device,
                                      vc4_device_to_handle(device),
                                      pCreateInfo, alloc, pSwapchain);
}

void
vc4_DestroySwapchainKHR(VkDevice _device,
                       VkSwapchainKHR swapchain,
                       const VkAllocationCallbacks *pAllocator)
{
   VC4_FROM_HANDLE(vc4_device, device, _device);
   const VkAllocationCallbacks *alloc;

   if (pAllocator)
      alloc = pAllocator;
   else
      alloc = &device->vk.alloc;

   wsi_common_destroy_swapchain(_device, swapchain, alloc);
}

VkResult
vc4_GetSwapchainImagesKHR(VkDevice device,
                         VkSwapchainKHR swapchain,
                         uint32_t *pSwapchainImageCount,
                         VkImage *pSwapchainImages)
{
   return wsi_common_get_images(swapchain, pSwapchainImageCount,
                                pSwapchainImages);
}

VkResult
vc4_AcquireNextImageKHR(VkDevice device,
                       VkSwapchainKHR swapchain,
                       uint64_t timeout,
                       VkSemaphore semaphore,
                       VkFence fence,
                       uint32_t *pImageIndex)
{
   VkAcquireNextImageInfoKHR acquire_info = {
      .sType = VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR,
      .swapchain = swapchain,
      .timeout = timeout,
      .semaphore = semaphore,
      .fence = fence,
      .deviceMask = 0,
   };

   return vc4_AcquireNextImage2KHR(device, &acquire_info, pImageIndex);
}

VkResult
vc4_AcquireNextImage2KHR(VkDevice _device,
                        const VkAcquireNextImageInfoKHR *pAcquireInfo,
                        uint32_t *pImageIndex)
{
   VC4_FROM_HANDLE(vc4_device, device, _device);
   struct vc4_physical_device *pdevice = device->physical_device;

   VkResult result = wsi_common_acquire_next_image2(
      &pdevice->wsi_device, _device, pAcquireInfo, pImageIndex);

   /* TODO signal fence and semaphore */

   return result;
}

VkResult
vc4_QueuePresentKHR(VkQueue _queue, const VkPresentInfoKHR *pPresentInfo)
{
   VC4_FROM_HANDLE(vc4_queue, queue, _queue);
   return wsi_common_queue_present(
      &queue->device->physical_device->wsi_device,
      vc4_device_to_handle(queue->device), _queue, queue->queue_family_index,
      pPresentInfo);
}

VkResult
vc4_GetDeviceGroupPresentCapabilitiesKHR(
   VkDevice device, VkDeviceGroupPresentCapabilitiesKHR *pCapabilities)
{
   memset(pCapabilities->presentMask, 0, sizeof(pCapabilities->presentMask));
   pCapabilities->presentMask[0] = 0x1;
   pCapabilities->modes = VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_BIT_KHR;

   return VK_SUCCESS;
}

VkResult
vc4_GetDeviceGroupSurfacePresentModesKHR(
   VkDevice device,
   VkSurfaceKHR surface,
   VkDeviceGroupPresentModeFlagsKHR *pModes)
{
   *pModes = VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_BIT_KHR;

   return VK_SUCCESS;
}

VkResult
vc4_GetPhysicalDevicePresentRectanglesKHR(VkPhysicalDevice physicalDevice,
                                         VkSurfaceKHR surface,
                                         uint32_t *pRectCount,
                                         VkRect2D *pRects)
{
   VC4_FROM_HANDLE(vc4_physical_device, device, physicalDevice);

   return wsi_common_get_present_rectangles(&device->wsi_device, surface,
                                            pRectCount, pRects);
}
