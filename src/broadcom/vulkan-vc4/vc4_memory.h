/*
 * Copyright (C) 2019-2020 Yaong <yaongtime@gmail.com>
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef VC4_MEMORY_H
#define VC4_MEMORY_H

VkResult vc4_vk_bo_init_alloc(struct vc4_device *dev, struct vc4_bo *bo, uint32_t size);
struct vc4_bo *vc4_vk_bo_alloc(struct vc4_device *dev, uint32_t size);
void vc4_vk_bo_free_mem(struct vc4_device *dev, struct vc4_bo *bo);
void vc4_vk_bo_free(struct vc4_device *dev, struct vc4_bo *bo);
int vc4_gem_export_dmabuf(const struct vc4_device *dev, uint32_t gem_handle);
VkResult vc4_bo_map(struct vc4_device *device, struct vc4_bo *bo);
void vc4_bo_unmap(struct vc4_device *device, struct vc4_bo *bo);
struct vc4_bo *vc4_bo_alloc_shader(struct vc4_device *device, const void *data, uint32_t size);

VkResult vc4_shader_bo_map(struct vc4_device *device, struct vc4_bo *bo);

#endif
